# Contents

[Working in this repository](#working-in-this-repository)
[Coding standards](#coding-standards)

## Working in this repository

### Creating a new model
Once you've installed and configured your development environment with `dbt`, you'll probably want to get cracking on building some new models!

Like a robust software development lifecycle, this involves prototyping safely within your own test schema, writing tests to be run in CI, and submitting a PR for review. By following the steps below, we bring the rigour of software engineering to our data pipelines:

1. Create a new branch from the latest `master` using the naming pattern `feature/JIRA-TICKET-NUM-description`
2. Decide where your new model belongs - naming and structure are important! Is it business facing (a `/models/mart`), or a back-end data cleaning model (a `/models/staging` table)?
  - You should read the excellent guide for [structuring dbt projects](https://discourse.getdbt.com/t/how-we-structure-our-dbt-projects/355), which underlies our model structure
3. Define your new model in the `.yml` file in its directory. At a bare minimum:
  - Provide the model name and description
  - Define the primary or surrogate key column and test it for `unique` and `not_null`
  - Define columns that have business logic applied to them (such as calculated metrics), using a separate `.md` file to reference more complex docs if necessary (see `production.yml` for examples)
  - Test any other columns using the schema tests if they have strict expectations: [`not_null`, `unique`, `accepted_values`]
4. Build your model by adding a `.sql` file to the corresponding location in the `/models` subdirectory
5. Build your model can build by running `dbt run -m @<your_model_name>` (the `@` should build your model's dependencies too)
6. Check that your tests pass by running `dbt test -m <your_model_name>`
7. Build all of the models and run the full test suit:
```zsh
dbt deps
dbt seed
dbt run --full-refresh # this will still only use sampled data since your local target should be `dev`
dbt test
```
8. Create a Pull Request to the master branch and request review from someone in the data team - you need an approved review before merging!

### Review checklist
To review a pull request on this repo, we summarise the setup process below:

1. Check that new tables have test coverage of key columns and any necessary business constraints
2. Check that new tables are correctly documented
3. Ensure that all tests have passed
4. Pull the branch and build all tables:
```zsh
dbt deps
dbt seed
dbt run --full-refresh # this will still only use sampled data since your local target should be `dev`
dbt test
```


## Coding standards
This style guide for `dbt` code uses the [dbt style guide](https://github.com/fishtown-analytics/corp/blob/master/dbt_coding_conventions.md) as its basis, with only minor divergences from it. If you would like to change one of our conventions, submit a PR - power to the people!

The conventions below might seem like a lot, but the next 80-odd lines of this readme help to save the team hours of time in the long run. Give these conventions a good read through and try to enforce them both in the code you write, and the PRs you review.

Consistency and clarity are often absent in the data world, but we can do better!

### Golden rules

- Never commit secrets, passwords, or any other sensitive data. If a password or key is committed by accident, it needs to be rotated. Keep your `profile.yml` local only, with the relevant connection details for your account.
- Never modify production tables directly - to change the structure or data of a table, you must go through the PR review process and write tests for your changes.
- Do not optimize for fewer lines of code. Newlines are cheap, brain time is expensive.

### Naming and field conventions

- Folder hierarchy: models are broadly split into two categories, with a prospective third category:
  1. `staging`: internal transformation tables, that are close to source data and serve the purpose of processing data in intermediate states.
  2. `marts`: business- or external-facing models to be used in data visualisations, reports, and API-served data. These tables (and their columns) should be named according to their business purpose, and in a way that a non-technical user can understand them.
  3. `ml`: tables prepared for re-use across different machine learning applications.
- Apart from `marts` tables (as discussed above), all schema, table and column names should be in snake_case. Convert to snake case by using the column enumeration in your SQL editor of choice (in DataGrip, this is under the `Alt+Enter` menu on a `select *`) to generate a baseline query, and then using something like the VSCode [change-case](https://github.com/wmaurer/vscode-change-case.git) extension (`Cmd+Shift+P -> 'Change case snake'`).
- In contrast, table names in all data marts (under `/models/marts`) should be based on the _business terminology_, rather than the source terminology. Think about a business end user trying to build a report and find the correct table and column. This also means using double-quotes to provide clear business-facing column names, e.g. "Time_spent_mins".
- Table names should be plurals, e.g. `users`.
- Each model should have a primary key, or composite key across several columns.
- The primary key of a model should be named <object>_id, e.g. account_id – this makes it easier to use using for joins (see below). _(One exception to the rule: we use `isbn` rather than `production.books.book_id` or `onix.products.product_id` due to the industry standard convention here.)_
- Timestamp columns should be named `<event>_at` (e.g. `publishingDate` would become `published_at`) and should be in UTC. If a different timezone is being used, this should be indicated with a suffix, e.g `created_at_pt`.
- Booleans should be prefixed with `is_` or `has_`.
- Price/revenue fields should be in decimal currency (e.g. 19.99 for $19.99; many app databases store prices as integers in cents). If non-decimal currency is used, indicate this with suffix, e.g. price_in_cents.
Avoid reserved words as column names
- Consistency is key! Use the same field names across models where possible, e.g. a key to the customers table should be named customer_id rather than user_id.

### CTEs
[Common Table Expressions (CTEs)]() provide a way to simplify SQL code by separating sequences of SQL transformations that might otherwise form a single highly complex query. Performance-wise, there is little penalty to using CTEs in general, as the query optimiser generally "passes through" the logic.

Some ground rules (derived from the dbt reference) help to make models easy to read:

- All {{ ref('...') }} statements should be placed in Common Table Expressions (CTEs) at the top of the file - think of them as imports
- Where performance permits, CTEs should perform a single, logical unit of work
- CTE names should be as verbose as needed to convey what they do
- CTEs that are duplicated across models should be pulled out into their own models
- CTEs should not change the grain of data (no `group by`)
- See example SQL query below for reference on CTE formatting 

### SQL style guide

- Indents should be **two spaces** (except for predicates, which should line up with the where keyword)
- Lines of SQL should be no longer than **80 characters**
- When listing columns, use **leading commas**; each line should start with a comma. This means that if you add columns in the middle or at the end of a list, the diff on git will only include new lines (but if you add one at the start it will also include the previous first column). A missing leading comma is also much easier to spot than a missing trailing comma.
- Field names and function names should all be **lowercase and unquoted**. Snowflake uppercases all unquoted object names by default, so the simplest method is to avoid using quotes and let the engine uppercase everything.
- The `as` keyword should be used when aliasing a field or table
- Fields should be stated before aggregates / window functions
- Ordering and grouping by a number (eg. group by 1, 2) rather than a column name is preferred. Note that if you are grouping by more than a few columns, you can use a macro to simplify this (see example).
- When possible, take advantage of `using` in joins
- Prefer `union all` to `union *`
- Avoid table aliases in join conditions (especially initialisms) – it's harder to understand what the table called "c" is compared to "customers".
- If joining two or more tables, always prefix your column names with the table alias. If only selecting from one table, prefixes are not needed.
- Be explicit about your join (i.e. write `inner join` instead of `join`). `left joins` are normally the most useful, `right joins` often indicate that you should change which table you select from and which one you join to.
- Add a space before and after function args - not all functions have simple syntax highlighting in VSCode or other code editors, so this can help to quickly read which functions are being used in large lists of columns, e.g. `rlike ( name, '.*books/[0-9]{13}.*' )`.

```
with my_data as (
  select * from {{ ref('my_data') }}
),

-- CTE comments go here
some_cte as (
  select * from {{ ref('some_cte') }}
),

final as (

  select [distinct]
    my_data.field_1
    , my_data.field_2
    , my_data.field_3

    -- use line breaks to visually separate calculations into blocks
    , case
        when my_data.cancellation_date is null
          or my_data.expiration_date is not null
        then expiration_date
        when my_data.cancellation_date is null
        then my_data.start_date + 7
        else my_data.cancellation_date
      end as cancellation_date

    -- use a line break before aggregations and space before function args
    , sum ( some_cte.field_4 )
    , max ( some_cte.field_5 )
    
    -- always use a unique_id to catch duplicates early and make testing easier
    , {{ dbt_utils.surrogate_key('field_1', 'field_2', 'field_3',
        'cancellation_date') }} as unique_id
  from my_data

  left join some_cte using ( id )

  where my_data.field_1 = 'abc'
  and (
    my_data.field_2 = 'def' or
    my_data.field_2 = 'ghi'
  )

  -- use macros where appropriate
  {{ dbt_utils.group_by(4) }} -- equivalent to: group by 1, 2, 3, 4

  having count(*) > 1

)

select * from final
```

### YAML style guide
- Indents should be **two spaces**
- List items should be indented
- Use a new line to separate list items that are dictionaries where appropriate
- Lines of YAML should be no longer than **80 characters** (split long descriptions by using double quotes and backslash)
- Use a line to separate table definitions, but otherwise don't add extra newlines
```
Example YAML
version: 2

models:
  - name: events
    columns:
      - name: event_id
        description: This is a unique identifier for the event
        tests:
          - unique
          - not_null
      - name: event_time
        description: "When the event occurred in UTC (eg. 2018-01-01 12:00:00)\
                      and here is a second line"
        tests:
          - not_null
      - name: user_id
        description: The ID of the user who recorded the event
        tests:
          - not_null
          - relationships:
              to: ref('users')
              field: id

  - name: visits_by_day # newline before new table
    columns:
      - name: unique_id # always have at least one unique column
        description: This is a unique identifier for the visit
        tests:
          - unique
          - not_null
```

### Jinja style guide
- When using Jinja delimiters, use spaces on the inside of your delimiter, like {{ this }} instead of {{this}}
- Use newlines to visually indicate logical blocks of Jinja

### Model building best practices and tips

- Use `{% if target.name == 'dev' %} {% endif %}` clauses to wrap conditions in your SQL queries on large tables so that you can prototype with a small subset of data. Faster queries, much more friendly to our Snowflake bill!
- Add at least (and probably exactly) one uniqueness constraint to every table. This will save you a huge amount of time in testing for duplicated values downstream. If no natural unique column exists, create a surrogate key column using a unique combination of other columns and the helper function `{{ dbt_utils.surrogate_key(*[column_list]*]) }}`.
- When prototyping, use patterns to selectively rebuild tables:
```zsh
# all models within a path under /models
dbt run -m 'marts.core.*' 

# all models and their children within a /models/staging/onix
dbt run -m 'staging.onix.*+' 
```
*Note: this syntax will match in `zsh`, if using bash the quotes are not necessary.*