{{
  config(
    materialized = "table"
  )
}}

with activity as (
  select 
   row_number() over (order by "timestamp") as location_id
   , "timestamp"::timestamp as location_at
    , parse_json(unnest_object.value) as flattened_activites
  from {{ ref('raw_location') }}
  , lateral flatten(input => "activity", outer => true) unnest_object
)

, parsed as (
  select
    location_id
    , location_at

    -- convert timestamp text to float, then divide by 1000 as given in milliseconds, and to_timestamp requires input in seconds
    , flattened_activites['timestamp']::datetime as activity_at
    
    -- convert type to lowercase
    , 'type' as type
    -- , lower(parse_json(activity_data.value['type'])) as type
    
    -- convert confidence to integer
    , parse_json(activity_data.value['confidence'])::int as confidence

  from activity
    , lateral flatten(input => flattened_activites['activity'], outer => true) activity_data
)

-- generating a row number for each activity type to force a unique id
, ranked as (
  select *
    , row_number() over (
        partition by location_id
        order by confidence desc, type
      ) as type_id
  from parsed
)

select *
    , {{ dbt_utils.generate_surrogate_key(['location_id', 'type_id']) }} as activity_id
from ranked