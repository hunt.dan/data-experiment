-- for this model, materialize as a table due to the expensive transformation on latitude and longitude
{{
  config(
    materialized='table',
    unique_key='unique_id',
    sort='location_at',
    tags=['google', 'location', 'raw']
  )
}}

with location as (
  select * from {{ ref('raw_location') }}
)

select 
  row_number() over (order by "timestamp") as location_id
  , "timestamp"::timestamp_ntz as location_at
  , "accuracy" as accuracy
  , "source" as source

  --  convert to decimal degrees
  , "latitudeE7"::number / 10000000 as latitude
  , case
      -- for occasional values outside of the range of -180 to 180, normalize to that range
      when "longitudeE7"::number / 10000000 > 180 then "longitudeE7"::number / 10000000 - 360
      when "longitudeE7"::number / 10000000 < -180 then "longitudeE7"::number / 10000000 + 360
      else "longitudeE7"::number / 10000000 
    end as longitude
  -- , velocity as velocity
  -- , heading as heading
  -- , altitude as altitude
  -- , "verticalAccuracy" as vertical_accuracy

  -- unique surrogate id
  , {{ dbt_utils.generate_surrogate_key(['row_number() over (order by "timestamp")', '"timestamp"']) }} as unique_id
from location