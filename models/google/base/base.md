{% docs base_location %}

  This contains the raw location data, with no structural changes. The only difference is that this view does not contain the 'activity' column, as it is captured in a table (below). The granularity of this table is one row per timestamp. All numerical columns have been captured in the raw data as floats, and the timestamp in Postgres timestamp format, so no data type conversions have been made in this model.

{% enddocs %}

{% docs base_activity %}

  This contains the data stored in a column ('activity') within the raw google location data. Activity data was stored in a nested json format, which has been 'unnested' in this table. The granularity of this table is one row per location_id and activity type, however due to some cases where there are multiple 'unknown' activities, a row number is generated for each activity type to force uniqueness. This is used to generated a unique surrogate key for this table.

  As the activity data was stored as a json-format string, the timestamp and confidence columns are converted to float types in this model. The activity timestamp is captured as a Unix / Epoch time (in milliseconds), so is divided by 1000 and converted to a Postgres timestamp.



{% enddocs %}