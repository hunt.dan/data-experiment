{% docs raw_location %}
Raw location data from Google Location History, stored with one row per log entry. This is the raw data parsed directly from the Records.json data, and forms the base for later transformations.
{% enddocs %}

{% docs location_geo_model %}
Adds encoding of locations as GIS points to the `base_location` model, which allows for location-based joins and functionality (e.g. distance). This model also adds rounded location fields, and rounded timestamps to the nearest 30min as well as week, all with the intent of simplifying downstream data use for the purpose of visualisation.

Note: doing joins with location information is mostly too compute-expensive on 1m+ rows. This size means that the tables can't fit in memory, and joins are therefore exponentially slower, so it's best avoided for timestamp level data. Also, avoid using spatial joins for views - push the work into materialised models so that it is only done once and doesn't affect query-time performance.

{% enddocs %}

{% docs location_anchor_ranges %}
Stores the mode() location for each week, as well as the range in latitude and longitude. These are practical for visualisation purposes, e.g. when deciding on the scale and centre point for a map. This dataset is also augmented with the closest city (>15k population dataset) which may be helpful for high-level summary data (e.g. travel over time).

{% enddocs %}

{% docs fact_location %}
Location data, stored with one row per log entry. In this fact table, the baseline latitude and longitude data is augmented with the most frequently logged location for each week (effectively a "home" location, e.g. for centering a map), and nearest city information of this "home" location. This is included in the fact table as joins on geo columns are expensive, so pushing this work into the original fact improves performances of downstream models aggregating over this data.

{% enddocs %}

{% docs rounded_latitude %}
Latitude rounded to a practical level for visualisation (3dp).

Specifically latitude is rounded to 3 decimal places, as this captures the granularity of a change in roughly one small city block (~111m), although this will change with latitude due to its variable size.

{% enddocs %}

{% enddocs %}

{% docs rounded_longitude %}
Longitude rounded to a practical level for visualisation (3dp).

Specifically longitude is rounded to 3 decimal places, as this captures the granularity of a change in roughly one small city block (~111m).

{% enddocs %}

{% enddocs %}

{% docs location_geo %}
Geo-encoded point for this location, which can be used for extended PostGIS functionality such as joining to nearest point.

{% enddocs %}

{% docs location_at_30min_interval %}
Location timestamp rounded to nearest 30mins, for the purpose of temporal visualisations of location data over longer time frames (days to months).

{% enddocs %}

{% docs location_date_week %}
Location date truncated to the week level, for the purpose of creating average location anchor points for a map over longer time frames (days to months).

{% enddocs %}

{% docs mode_city %}
Closest city to the most frequent latitude, longitude for a given week, i.e. "home" city for the week.
{% enddocs %}

{% docs mode_latitude %}
Most frequent latitude for a given week, i.e. "home" latitude for the week.
{% enddocs %}
{% docs mode_longitude %}
Most frequent longitude for a given week, i.e. "home" longitude for the week.
{% enddocs %}
{% docs range_latitude %}
Range between min and max latitude for a given week.
{% enddocs %}
{% docs range_longitude %}
Range between min and max longitude for a given week.
{% enddocs %}

