{{
  config({
    "materialized" : "table"
    })
}}

with location_geo as (
  select * from {{ ref('location_geo') }}
),

world_cities as (
  select * from {{ ref('seed_world_cities_geo') }}
),

-- get the mode() aggregated for lat and long for each week
aggregated as (
  select
    location_date_week

    -- focal point to help centre a map, "home"
    , mode(rounded_latitude) as mode_latitude
    , mode(rounded_longitude) as mode_longitude

    -- ranges that may help to zoom a map
    , abs(max(rounded_latitude) - min(rounded_latitude)) as range_latitude
    , abs(max(rounded_longitude) - min(rounded_longitude)) as range_longitude
  from location_geo
  group by location_date_week
),

-- re-calculate geo for mode values; mode() doesn't function over geo columns
--  => slightly inefficient but low data volumes post-aggregation
geo_augmented as (
  select *

    -- create a geometric representation to allow spatial joins
    , ST_MakePoint(mode_longitude, mode_latitude) as mode_location_geo
  from aggregated
),

nearest_city as (
  select geo.*
    
    -- add nearest city data
    , cities.city as mode_city
    , cities.country_iso_code as mode_country_iso_code
    , cities.region as mode_region
  from geo_augmented geo
  -- get closest city
  join world_cities cities
    on true -- lateral join with open condition, but filtered to 1 row above
  -- keep only the closest city
  qualify 1 = row_number() over (
    partition by geo.location_date_week
    order by st_distance(geo.mode_location_geo, cities.location_geo)
  )
)


select *
from nearest_city
