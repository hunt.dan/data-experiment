{{
  config({
    "materialized" : "table"
    })
}}

with location as (
  select * from {{ ref('fact_location') }}
)

select
  rounded_latitude
  , rounded_longitude

  -- aggregate metrics
  , min(location_at) as earliest_record
  , max(location_at) as latest_record
  , count(location_id) as number_of_points

  -- will be used to help determine 'bad data' points
  , avg(accuracy) as average_accuracy
  -- , avg(velocity) as average_velocity

  -- unique surrogate id
  , {{ dbt_utils.generate_surrogate_key(['rounded_latitude', 'rounded_longitude']) }} as unique_id

from location
group by rounded_latitude
  , rounded_longitude
order by rounded_latitude
  , rounded_longitude