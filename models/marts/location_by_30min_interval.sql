{{
  config({
    "materialized" : "table"
    })
}}

with location as (
  select * from {{ ref('fact_location') }}
)

select
  -- rounded timestamp, which should be unique
  location_at_30min_interval

  -- all other fields should only have an aggregate value
  -- map anchor range
  , max(mode_latitude) as mode_latitude
  , max(mode_longitude) as mode_longitude
  , max(range_latitude) as range_latitude
  , max(range_longitude) as range_longitude

  -- helpful info for viz labels / groupings
  , max(mode_city) as mode_city
  , max(mode_country_iso_code) as mode_country_iso_code

  -- average location for the interval
  , avg(rounded_latitude) as rounded_latitude
  , avg(rounded_longitude) as rounded_longitude

  -- aggregate metrics to help filter bad data
  , avg(accuracy) as average_accuracy
  -- , avg(velocity) as average_velocity
  
  , count(*) as number_of_points

from location
group by 1
order by 1, 2, 3