"""
This file is a multi-loader that can be run in order to load data from all files present on the local filesystem.

This loader is designed to find all files under `scripts/loaders/*`, import each one and execute its class' `load()` method, and then write the output to a database table under the `DB_SCHEMA` configured in the `.env` file.

To add a new data loader:
1.  Add the loader class file under a path like `scripts/loaders/<domain>/<dataset>.py` (e.g. `scripts/loaders/google/location.py`)
2.  Import your class in `main.py`:
```from scripts.loaders.google.location import GoogleLocationDataLoader)```
3.  Update `main.py` to recognise your new path under the `_get_data_loader(path)` function:
```
if 'google/location' in path:
    return GoogleLocationDataLoader()
```
"""

import os
import glob
import re

# loader classes
from scripts.loaders.google.location import GoogleLocationDataLoader
from scripts.loaders.fitbit.distance import FitbitDistanceDataLoader
from scripts.loaders.fitbit.heart_rate import FitbitHeartRateDataLoader
from scripts.loaders.fitbit.sleep import FitbitSleepDataLoader
from scripts.loaders.fitbit.steps import FitbitStepsDataLoader

# from scripts.loaders.facebook import *

ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
LOADERS_PATH = ROOT_PATH + '/scripts/loaders'

def _get_data_loader(path):
  if 'google/location' in path:
    return GoogleLocationDataLoader()
  elif 'google/hangouts' in path:
    raise NotImplementedError(path)
  elif 'facebook/hangouts' in path:
    raise NotImplementedError(path)
  elif 'fitbit/distance' in path:
    return FitbitDistanceDataLoader()
  elif 'fitbit/heart_rate' in path:
    return FitbitHeartRateDataLoader()
  elif 'fitbit/sleep' in path:
    return FitbitSleepDataLoader()
  elif 'fitbit/steps' in path:
    return FitbitStepsDataLoader()
  else:
    raise ValueError(path)

print(LOADERS_PATH)
print(glob.glob('./scripts/loaders/*/**.py', recursive=True))

for path in glob.glob('./scripts/loaders/*/**.py', recursive=True):
  try:
    print(path)
    # import the class for this path
    dataloaderClass = _get_data_loader(path)

    # get dataframe with all data for this dataset
    df = dataloaderClass.load()

    # parse subdirectory and filename to use as tablename (e.g. google_location)
    tablename_search = re.search('scripts/loaders/(\w+)/(\w+).py', path, re.IGNORECASE)

    if tablename_search and df is not None:
      tablename = tablename_search.group(1) + '_' + tablename_search.group(2)
      print(tablename)

      # if name was parsed, write out file
      print('\nWriting data to database')
      dataloaderClass.write(df, tablename)
    else:
      print('Could not parse name in correct format from file path:' + path) 
  except Exception as error:
    print(str(error))
