# grant the loader role permissions to add to the JSON stage
resource "snowflake_stage_grant" "read_json" {
  database_name = snowflake_database.raw.name
  schema_name   = snowflake_schema.stages_local.name
  privilege     = "READ"
  roles         = [snowflake_role.datahunt.name]
  stage_name    = snowflake_stage.local_json_stage.name
}

# grant the loader role permissions to add to the JSON stage
# needs read_json grant before it can be applied
resource "snowflake_stage_grant" "write_json" {
  database_name = snowflake_database.raw.name
  schema_name   = snowflake_schema.stages_local.name
  privilege     = "WRITE"
  roles         = [snowflake_role.datahunt.name]
  stage_name    = snowflake_stage.local_json_stage.name

  depends_on = [
    snowflake_stage_grant.read_json
  ]
}
