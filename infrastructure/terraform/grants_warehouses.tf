# Grant privileges on warehouses

resource "snowflake_warehouse_grant" "loading_usage" {
  privilege      = "USAGE"
  roles          = [snowflake_role.datahunt.name]
  warehouse_name = snowflake_warehouse.loading.name
}

resource "snowflake_warehouse_grant" "transforming_usage" {
  privilege = "USAGE"
  roles = [snowflake_role.datahunt.name]
  warehouse_name = snowflake_warehouse.transforming.name
}
