# stage where local data in JSON format can be loaded to Snowflake
# Note: 
# 1. Grant is messy due to poor support for file formats in the current provider
# 2. Changes to file format are ignored for the same reason (otherwise this appears in every plan)
resource "snowflake_stage" "local_json_stage" {
  provider = snowflake
  name     = "JSON_STAGE"
  database = snowflake_database.raw.name
  schema   = snowflake_schema.stages_local.name
  file_format = format("FORMAT_NAME =  %s", join(".", [
    snowflake_file_format.json_file_format.database,
    snowflake_file_format.json_file_format.schema,
    snowflake_file_format.json_file_format.name
    ]
  ))
  lifecycle {
    ignore_changes = [
      file_format
    ]
  }
}