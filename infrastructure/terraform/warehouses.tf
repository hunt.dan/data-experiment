# warehouses allow for isolated scaling of different workloads

# warehouse for running snowpark commands, which will have access to more memory
resource "snowflake_warehouse" "loading" {
  name                                = "LOADING"
  warehouse_size                      = "MEDIUM"
  warehouse_type                      = "SNOWPARK-OPTIMIZED"
  auto_suspend                        = 60
  auto_resume                         = true
  initially_suspended                 = true
  query_acceleration_max_scale_factor = 0

  lifecycle {
    ignore_changes = [
      # ignore changes to the warehouse type, as right now this doesn't account for snowpark-optimized warehouses
      warehouse_type
    ]
  }
}

# warehouse for developers or transformation tools performing data transformations or ad hoc queries
resource "snowflake_warehouse" "transforming" {
  name                                = "TRANSFORMING"
  warehouse_size                      = "xsmall"
  auto_suspend                        = 60
  auto_resume                         = true
  initially_suspended                 = true
  query_acceleration_max_scale_factor = 0
}
