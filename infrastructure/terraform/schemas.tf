# schema used for stages where local data can be uploaded to Snowflake
resource "snowflake_schema" "stages_local" {
  name     = "STAGES_LOCAL"
  database = snowflake_database.raw.name
}