# grant usage to schemas for loader, transformer
resource "snowflake_schema_grant" "raw_stages_local_usage" {
  database_name = snowflake_database.raw.name
  schema_name   = snowflake_schema.stages_local.name
  privilege     = "USAGE"
  roles         = [snowflake_role.datahunt.name]
}

# grant create table for loader
resource "snowflake_schema_grant" "raw_stages_local_create_table" {
  database_name = snowflake_database.raw.name
  schema_name   = snowflake_schema.stages_local.name
  privilege     = "CREATE TABLE"
  roles         = [snowflake_role.datahunt.name]
}

# grant create view for loader
resource "snowflake_schema_grant" "raw_stages_local_create_view" {
  database_name = snowflake_database.raw.name
  schema_name   = snowflake_schema.stages_local.name
  privilege     = "CREATE VIEW"
  roles         = [snowflake_role.datahunt.name]
}

# grant usage to schemas for 
resource "snowflake_schema_grant" "raw_stages_local_create_function" {
  database_name = snowflake_database.raw.name
  schema_name   = snowflake_schema.stages_local.name
  privilege     = "CREATE PROCEDURE"
  roles         = [snowflake_role.datahunt.name]
}

# grant usage to schemas for 
resource "snowflake_schema_grant" "raw_stages_local_create_file_format" {
  database_name = snowflake_database.raw.name
  schema_name   = snowflake_schema.stages_local.name
  privilege     = "CREATE FILE FORMAT"
  roles         = [snowflake_role.datahunt.name]
}

# grant usage to schemas for
resource "snowflake_procedure_grant" "raw_stages_local_use_stored_procedure" {
  database_name = snowflake_database.raw.name
  schema_name   = snowflake_schema.stages_local.name
  privilege     = "USAGE"
  roles         = [snowflake_role.datahunt.name]
  on_future     = true
}
