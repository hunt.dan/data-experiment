# user to perform data loading and transformation throughout the warehouse
resource "snowflake_user" "user_datahunt" {
  name                 = "ELT_USER"
  password             = "DUMMY_PASSWORD_MUST_CHANGE"
  default_warehouse    = snowflake_warehouse.loading.name
  default_role         = snowflake_role.datahunt.name
  must_change_password = true
}

